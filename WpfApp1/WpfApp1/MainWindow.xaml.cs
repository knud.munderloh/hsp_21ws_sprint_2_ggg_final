﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    //Eigenschaften der Mutter



    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Eingabe
        public int Mutterntyp;
        public int Gewindeart;
        public double Nenndurchmesser;
        public int NenndurchmesserInt;
        public int Normmutter;
        public int Material;
        public double Schlüsselweite;
        public double Höhe;
        public double Gewindesteigung;
        public int Gewinderichtung;
        public int Stückzahl;

        //Ausgabe
        public double maxDurchmesser;
        public double minDurchmesser;
        public double Gewicht;
        public double Volumen;
        public double Flankendurchmesser;
        public double Gewindetiefe;
        public double Preis;

        //Hilfsvariablen
        public double[] Dichte = new double[7] { 0, 7.85, 7.9, 8.0, 8.73, 2.7, 4.507 };   //Dichte in g/cm^3
        public double[] Materialpreis = new double[7] { 0, 0.02, 0.03, 0.04, 0.035, 0.04, 0.010 };          //Euro pro Gramm (Angaben sind ausgedacht)
        public int Fehler;
        public String Typ;
        public String verfuegbare_nenndurchmesser ="";
        public double[] FlankendurchmesserArrayRegel = new double[18] { 1.740, 2.675, 3.545, 4.480, 5.350, 7.188, 9.026, 10.863, 12.701, 14.701, 16.376, 18.376, 20.376, 22.051, 25.051, 27.727, 30.727, 33.402 };
        public double[] GewindesteigungArrayRegel = new double[18] { 0.40, 0.50, 0.70, 0.80, 1.00, 1.25, 1.50, 1.75, 2.00, 2.00, 2.50, 2.50, 2.50, 3.00, 3.00, 3.50, 3.50, 4.00 };



        //Datensätze

        //Datensatz Sechskantmutter 

        public Mutter M2 = new Mutter(2, 4, 3.1, 1.6, 0.4, 1.740, 0.245);
        public Mutter M3 = new Mutter(3, 5.5, 4.6, 2.4, 0.5, 2.675, 0.307);
        public Mutter M4 = new Mutter(4, 7, 5.9, 3.2, 0.7, 3.545, 0.429);
        public Mutter M5 = new Mutter(5, 8, 6.9, 4.7, 0.8, 4.480, 0.491);
        public Mutter M6 = new Mutter(6, 10, 8.9, 5.2, 1, 5.350, 0.613);
        public Mutter M8 = new Mutter(8, 13, 11.6, 6.8, 1.25, 7.188, 0.767);
        public Mutter M10 = new Mutter(10, 16, 14.6, 8.4, 1.5, 8.188, 0.920);
        public Mutter M12 = new Mutter(12, 18, 16.6, 10.8, 1.75, 10.863, 1.074);
        public Mutter M16 = new Mutter(16, 24, 22.5, 14.8, 2, 14.701, 1.227);
        public Mutter M20 = new Mutter(20, 30, 27.7, 18, 2.5, 18.376, 1.534);
        public Mutter M24 = new Mutter(24, 36, 33.3, 21.5, 3, 22.051, 1.840);
        public Mutter M30 = new Mutter(30, 46, 42.8, 25.6, 3.5, 27.727, 1.894);
        public Mutter M36 = new Mutter(36, 55, 51.1, 31, 4, 33.402, 2.147);

        //Datensatz Vierkantmutter
        public Mutter M5_vierkant = new Mutter(5, 8, 6.7, 4, 0.8);
        public Mutter M6_vierkant = new Mutter(6, 10, 8.7, 5, 1);
        public Mutter M8_vierkant = new Mutter(8, 13, 11.5, 6.5, 1.25);
        public Mutter M10_vierkant = new Mutter(16, 17, 15.5, 8, 1.5);
        public Mutter M12_vierkant = new Mutter(18, 19, 17.2, 10, 1.75);
        public Mutter M16_vierkant = new Mutter(16, 24, 22, 13, 2);

        //Datensatz Flanschmutter
        public Mutter M5_flansch = new Mutter(5, 8, 9.8, 5, 0.8);
        public Mutter M6_flansch = new Mutter(6, 10, 12.2, 6, 1);
        public Mutter M8_flansch = new Mutter(8, 13, 15.8, 8, 1.25);
        public Mutter M10_flansch = new Mutter(10, 16, 19.6, 10, 1.5);
        public Mutter M12_flansch = new Mutter(12, 18, 23.8, 12, 1.75);
        public Mutter M16_flansch = new Mutter(16, 24, 31.9, 16, 2);
        public Mutter M20_flansch = new Mutter(20, 30, 39.9, 20, 2.5);

        //Datensatz Hutmutter
        public Mutter M4_hut = new Mutter(4, 7, 5.9, 8, 0.7, 5.74);
        public Mutter M5_hut = new Mutter(5, 8, 6.9, 10, 0.8, 7.79);
        public Mutter M6_hut = new Mutter(6, 10, 8.9, 12, 1, 8.29);
        public Mutter M8_hut = new Mutter(8, 13, 11.6, 15, 1.25, 11.35);
        public Mutter M10_hut = new Mutter(10, 16, 14.6, 18, 1.5, 13.35);
        public Mutter M12_hut = new Mutter(12, 18, 16.6, 22, 1.75, 16.35);
        public Mutter M16_hut = new Mutter(16, 24, 22.5, 28, 2, 21.42);
        public Mutter M20_hut = new Mutter(20, 30, 27.7, 34, 2.5, 26.42);
        public Mutter M24_hut = new Mutter(24, 36, 33.3, 42, 3, 31.50);

        // Platzhalter

        public Mutter mutter_ausgewählt = new Mutter();

        //Collections in denen die Datensätze enthalten sind
        Hashtable liste_sechskant = new Hashtable();
        Hashtable liste_vierkant = new Hashtable();
        Hashtable liste_flansch = new Hashtable();
        Hashtable liste_hut = new Hashtable();

        public MainWindow()
        {
            InitializeComponent();

            //START Arbeitserleichterung
            Lbl_Nenndurchmesser.Visibility = Visibility.Collapsed;
            Cb_Nenndurchmesser.Visibility = Visibility.Collapsed;
            Lbl_Material.Visibility = Visibility.Collapsed;
            Cb_Material.Visibility = Visibility.Collapsed;
            Lbl_Stückzahl.Visibility = Visibility.Collapsed;
            Tb_Stückzahl.Visibility = Visibility.Collapsed;
            Lbl_Gewindeart.Visibility = Visibility.Collapsed;
            Cb__Gewindeart.Visibility = Visibility.Collapsed;
            Lbl_Gewinderichtung.Visibility = Visibility.Collapsed;
            Cb_Gewinderichtung.Visibility = Visibility.Collapsed;

            Lbl_Gewindesteigung.Visibility = Visibility.Collapsed;
            Tb_Gewindesteigung.Visibility = Visibility.Collapsed;
            Lbl_Höhe.Visibility = Visibility.Collapsed;
            Tb_Höhe.Visibility = Visibility.Collapsed;
            Lbl_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Schlüsselweite.Visibility = Visibility.Collapsed;
            //ENDE Arbeitserleichterung

            /*Datensätze in Collection einpflegen
            //Hashtable erwartet einen Key (String) und einen Value (Objekt Mutter)
            //Im Feld für Key steht der selbe Wert wie in der ComboBox
            So lassen sich die Daten direkt auslesen
            */

            //Datensätze Sechskantmutter
            liste_sechskant.Add("M2", M2);
            liste_sechskant.Add("M3", M3);
            liste_sechskant.Add("M4", M4);
            liste_sechskant.Add("M5", M5);
            liste_sechskant.Add("M6", M6);
            liste_sechskant.Add("M8", M8);
            liste_sechskant.Add("M10", M10);
            liste_sechskant.Add("M12", M12);
            liste_sechskant.Add("M16", M16);
            liste_sechskant.Add("M20", M20);
            liste_sechskant.Add("M24", M24);
            liste_sechskant.Add("M30", M30);
            liste_sechskant.Add("M36", M36);

            //Datensätze Vierkantmutter
            liste_vierkant.Add("M5", M5_vierkant);
            liste_vierkant.Add("M6", M6_vierkant);
            liste_vierkant.Add("M8", M8_vierkant);
            liste_vierkant.Add("M10", M10_vierkant);
            liste_vierkant.Add("M12", M12_vierkant);
            liste_vierkant.Add("M16", M16_vierkant);

            //Datensätze Flanschmutter

            liste_flansch.Add("M5", M5_flansch);
            liste_flansch.Add("M6", M6_flansch);
            liste_flansch.Add("M8", M8_flansch);
            liste_flansch.Add("M10", M10_flansch);
            liste_flansch.Add("M12", M12_flansch);
            liste_flansch.Add("M16", M16_flansch);
            liste_flansch.Add("M20", M20_flansch);

            //Datensätze Hutmutter

            liste_hut.Add("M4", M4_hut);
            liste_hut.Add("M5", M5_hut);
            liste_hut.Add("M6", M6_hut);
            liste_hut.Add("M8", M8_hut);
            liste_hut.Add("M10", M10_hut);
            liste_hut.Add("M12", M12_hut);
            liste_hut.Add("M16", M16_hut);
            liste_hut.Add("M20", M20_hut);
            liste_hut.Add("M24", M24_hut);
        }

        //Buttons

        private void Btn_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Btn_Hilfe_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Zu Risiken und Nebenwirkungen fragen Sie bitte Ihren Arzt oder Apotheker.");
            // Funny Joke einfügen
        }

       public void Btn_Berechnung_Click(object sender, RoutedEventArgs e)
        {
            // 
            mutter_ausgewählt = gewählte_mutter();
            minDurchmesser = mutter_ausgewählt.getNenndurchmesser();
            maxDurchmesser = Nenndurchmesser + 0.51;
            Gewindesteigung= Nenndurchmesser * 0.1;
            mutter_ausgewählt.setGewindesteigung(Gewindesteigung);
            mutter_ausgewählt.setmaxDurchmesser(maxDurchmesser);
            mutter_ausgewählt.setGewindesteigung(GewindesteigungArrayRegel[Cb_Nenndurchmesser.SelectedIndex]);


            if (Rb_Normmutter.IsChecked==true)
            {
                mutter_ausgewählt = gewählte_mutter();
                
                if (mutter_ausgewählt == null)
                {
                    verfuegbare_nenndurchmesser = "";
                    switch (Mutterntyp)
                    {
                        
                        case 0:
                            Typ = "Sechskant";
                            foreach (string key in liste_sechskant.Keys)
                            {
                                verfuegbare_nenndurchmesser += key + "; ";
                            }
                            break;
                        case 1:
                            Typ = "Vierkant";
                            foreach (string key in liste_vierkant.Keys)
                            {
                                verfuegbare_nenndurchmesser += key + "; ";
                            }
                            break;
                        case 2:
                            Typ = "Flansch";
                            foreach (string key in liste_flansch.Keys)
                            {
                                verfuegbare_nenndurchmesser += key + "; ";
                            }
                            break;
                        case 3:
                            Typ = "Hut";
                            foreach (string key in liste_hut.Keys)
                            {
                                verfuegbare_nenndurchmesser += key + "; ";
                            }
                            break;
                    }
                    MessageBox.Show("Der Nenndurchmesser " + Cb_Nenndurchmesser.Text + " ist in der Norm der " + Typ + "mutter nicht verfügbar. Bitte einen anderen Nenndurchmesser auswählen. Verfügbare Nenndurchmesser für " + Typ + ": \n" + verfuegbare_nenndurchmesser);
                }
                else
                {
                    if (Fehler == 1 || mutter_ausgewählt.Schlüsselweite <= 1.2 * mutter_ausgewählt.Nenndurchmesser || Cb_Nenndurchmesser.Text.Equals("") || Cb_Material.Text.Equals("") || Cb__Gewindeart.Text.Equals(""))
                    {
                        MessageBox.Show("Es liegt eine Fehlerhafte Eingabe vor!");
                        Lbl_Fehler.Visibility = Visibility.Visible;
                    }

                    else
                    {
                        Lbl_Fehler.Visibility = Visibility.Hidden;
                        

                        calcAll(mutter_ausgewählt);
                        minDurchmesser = mutter_ausgewählt.getNenndurchmesser();
                        maxDurchmesser = Nenndurchmesser + 0.51;

                        mutter_ausgewählt.setmaxDurchmesser(maxDurchmesser);
                        minDurchmesser = mutter_ausgewählt.calcminGewindedurchmesser();
                        mutter_ausgewählt.calcRegelgewinde();

                        Tbl_maxDurchmesser.Text = Convert.ToString(maxDurchmesser);
                        Tbl_Volumen.Text = Convert.ToString(mutter_ausgewählt.calcVolumen(maxDurchmesser, mutter_ausgewählt.maxGewindedurchmesser, mutter_ausgewählt.minGewindedurchmesser, Schlüsselweite, Mutterntyp, 0));
                        Tbl_Gewicht.Text = Convert.ToString(mutter_ausgewählt.calcGewicht(Mutterntyp, Cb_Material.SelectedIndex));
                        Preis = Math.Round(Convert.ToDouble(Tbl_Gewicht.Text) * 2.21, 2);
                        Tbl_Preis.Text = Convert.ToString(Preis * Convert.ToInt16(Tb_Stückzahl.Text)) + "€";
                        Tbl_Gewindesteigung.Text = Convert.ToString(Gewindesteigung);
                        Tbl_Schlüsselweite.Text = Convert.ToString(Schlüsselweite);
                        Tbl_Höhe.Text = Convert.ToString(Höhe);
                        Tbl_Gewindetiefe.Text = Convert.ToString(Gewindetiefe);
                        Tbl_Preis.Text = Convert.ToString(Preis);
                        Tbl_Höhe.Text = Convert.ToString(mutter_ausgewählt.calcHöhe(Mutterntyp));
                        Tbl_Gewindesteigung.Text = Convert.ToString(GewindesteigungArrayRegel[Cb_Nenndurchmesser.SelectedIndex]);

                        Flankendurchmesser = FlankendurchmesserArrayRegel[Cb_Nenndurchmesser.SelectedIndex];

                        if (Flankendurchmesser == 0)
                        {
                            Tbl_Flankendurchmesser.Text = "Keine Angabe!";
                        }
                        else
                        {
                            Tbl_Flankendurchmesser.Text = Convert.ToString(Flankendurchmesser);
                        }
                        if (Gewindetiefe == 0)
                        {
                            Tbl_Gewindetiefe.Text = "Keine Angabe!";
                        }

                        

                        Grd_Ausgabe.Visibility = Visibility.Visible;
                    }
                }
            }
            else if (Rb_Spezifizierbar.IsChecked == true)
            {
                resetGUI(); 
                Höhe = Convert.ToDouble(Tb_Höhe.Text);
                mutter_ausgewählt.setHöhe(Höhe);
                Gewindesteigung = (GewindesteigungArrayRegel[Cb_Nenndurchmesser.SelectedIndex]);
                // Hier Berechnung und Anzeige von Spezifizierbar einfügen
                Flankendurchmesser = FlankendurchmesserArrayRegel[Cb_Nenndurchmesser.SelectedIndex];
                Tbl_Flankendurchmesser.Text = Convert.ToString(Flankendurchmesser);
                minDurchmesser = mutter_ausgewählt.calcminGewindedurchmesser();
                mutter_ausgewählt.calcRegelgewinde();
                Tbl_Höhe.Text = Tb_Höhe.Text;
                Tbl_Schlüsselweite.Text = Convert.ToString(mutter_ausgewählt.calcSchlüsselweite(Mutterntyp));
                Stückzahl = Convert.ToInt16(Tb_Stückzahl.Text);
                
                Tbl_maxDurchmesser.Text = Convert.ToString(mutter_ausgewählt.calcmaxDurchmesser(Mutterntyp));
                

                mutter_ausgewählt.setNenndurchmesser(Nenndurchmesser);
                

                Tbl_Gewindesteigung.Text = Convert.ToString(Gewindesteigung);
                



                Tbl_Volumen.Text = Convert.ToString(mutter_ausgewählt.calcVolumen(maxDurchmesser, mutter_ausgewählt.maxGewindedurchmesser, mutter_ausgewählt.minGewindedurchmesser, Schlüsselweite, Mutterntyp, 2));
                
                Tbl_Gewicht.Text = Convert.ToString(mutter_ausgewählt.calcGewicht(Mutterntyp, Cb_Material.SelectedIndex));
                Preis = Math.Round(Convert.ToDouble(Tbl_Gewicht.Text) * 2.21, 2);
                Tbl_Preis.Text = Convert.ToString(Preis* Convert.ToInt16(Tb_Stückzahl.Text))+"€";


                Tbl_Gewindetiefe.Text= Convert.ToString(0.61343 * Gewindesteigung);


            }
            else if (Rb_Spezialanfertigung.IsChecked == true)
            {
                resetGUI();
                Höhe = Convert.ToDouble(Tb_Höhe.Text);
                mutter_ausgewählt.setHöhe(Höhe);
                // Hier Berechnung und Anzeige von Spezialanfertigung einfügen
                Flankendurchmesser = FlankendurchmesserArrayRegel[Cb_Nenndurchmesser.SelectedIndex];
                Tbl_Flankendurchmesser.Text = Convert.ToString(Flankendurchmesser);
                minDurchmesser = mutter_ausgewählt.calcminGewindedurchmesser();
                mutter_ausgewählt.calcRegelgewinde();

                Tbl_Schlüsselweite.Text = Tb_Schlüsselweite.Text;

                Tbl_Höhe.Text = Tb_Höhe.Text;
                
                Tbl_Gewindesteigung.Text = Tb_Gewindesteigung.Text;
                Gewindesteigung = Convert.ToDouble(Tbl_Gewindesteigung.Text);
                Tbl_Gewindetiefe.Text = Convert.ToString(0.61343 * Gewindesteigung);
                Stückzahl = Convert.ToInt16(Tb_Stückzahl.Text);

                Tbl_Volumen.Text = Convert.ToString(mutter_ausgewählt.calcVolumen(maxDurchmesser, mutter_ausgewählt.maxGewindedurchmesser, mutter_ausgewählt.minGewindedurchmesser, Schlüsselweite, Mutterntyp, 2));


                Tbl_Gewicht.Text = Convert.ToString(mutter_ausgewählt.calcGewicht(Mutterntyp, Cb_Material.SelectedIndex));

                

                Tbl_maxDurchmesser.Text = Convert.ToString(mutter_ausgewählt.calcmaxDurchmesser(Mutterntyp));
                Preis = Math.Round(Convert.ToDouble(Tbl_Gewicht.Text) * 2.21, 2);
                Tbl_Preis.Text = Convert.ToString(Preis * Convert.ToInt16(Tb_Stückzahl.Text)) + "€";







            }




        }

        public void resetGUI()
        {
            Mutterntyp = 0;
            Gewindeart = 0;
            Nenndurchmesser = 0;
            NenndurchmesserInt = 0;
            Normmutter = 0;
            Material = 0;
            Schlüsselweite = 0;
            Höhe = 0;
            Gewindesteigung = 0;
            Gewinderichtung = 0;
            Stückzahl = 0;

            //Ausgabe
            maxDurchmesser = 0;
            Gewicht = 0;
            Volumen = 0;
            Flankendurchmesser = 0;
            Gewindetiefe = 0;
            Preis = 0;


            Tbl_maxDurchmesser.Text = "";
            Tbl_Volumen.Text = "";
            Tbl_Gewicht.Text = "";
            Tbl_Gewindesteigung.Text = "";
            Tbl_Schlüsselweite.Text = "";
            Tbl_Höhe.Text = "";
            Tbl_Gewindetiefe.Text = "";
            Tbl_Preis.Text = "";
            Tbl_Flankendurchmesser.Text = "";
        }

        private Mutter gewählte_mutter() // Findet die Mutter, die vom Benutzer ausgewählt wurde und gibt sie zurück
        {
            switch (Mutterntyp)
            {
                case 0:
                    mutter_ausgewählt = (Mutter)liste_sechskant[Cb_Nenndurchmesser.Text];
                    break;
                case 1:
                    mutter_ausgewählt = (Mutter)liste_vierkant[Cb_Nenndurchmesser.Text];
                    break;
                case 2:
                    mutter_ausgewählt = (Mutter)liste_flansch[Cb_Nenndurchmesser.Text];
                    break;
                case 3:
                    mutter_ausgewählt = (Mutter)liste_hut[Cb_Nenndurchmesser.Text];
                    break;
            }
            return mutter_ausgewählt;
        }



        private void Btn_InDenWarenkorb_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Coming soon!");
            Ti_Auswahl.Focus();
            // Erstellte Mutter in den Warenkorb übergeben (nur eigegebene Daten)
        }

        //START NUTZERFÜHRUNG
        private void Tvi_Sechskantmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 0;
        }
        private void Tvi_Vierkantmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 1;
        }
        private void Tvi_Flanschmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 2;
        }
        private void Tvi_Hutmutter_Selected(object sender, RoutedEventArgs e)
        {
            Rb_Normmutter.Visibility = Visibility.Visible;
            Rb_Spezifizierbar.Visibility = Visibility.Visible;
            Rb_Spezialanfertigung.Visibility = Visibility.Visible;

            Mutterntyp = 3;
        }

        private void Rb_Normmutter_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 0;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;
            Lbl_Gewindeart.Visibility = Visibility.Visible;
            Cb__Gewindeart.Visibility = Visibility.Visible;
            Lbl_Gewinderichtung.Visibility = Visibility.Visible;
            Cb_Gewinderichtung.Visibility = Visibility.Visible;

            Lbl_Gewindesteigung.Visibility = Visibility.Collapsed;
            Tb_Gewindesteigung.Visibility = Visibility.Collapsed;
            Lbl_Höhe.Visibility = Visibility.Collapsed;
            Tb_Höhe.Visibility = Visibility.Collapsed;
            Lbl_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Schlüsselweite.Visibility = Visibility.Collapsed;
        
        }

        private void Spezifizierbar_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 1;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;
            Lbl_Gewindeart.Visibility = Visibility.Visible;
            Cb__Gewindeart.Visibility = Visibility.Visible;
            Lbl_Gewinderichtung.Visibility = Visibility.Visible;
            Cb_Gewinderichtung.Visibility = Visibility.Visible;
            Lbl_Höhe.Visibility = Visibility.Visible;
            Tb_Höhe.Visibility = Visibility.Visible;

            Lbl_Schlüsselweite.Visibility = Visibility.Collapsed;
            Tb_Schlüsselweite.Visibility = Visibility.Collapsed;
            Lbl_Gewindesteigung.Visibility = Visibility.Collapsed;
            Tb_Gewindesteigung.Visibility = Visibility.Collapsed;


        }

        private void Spezialanfertigung_Checked(object sender, RoutedEventArgs e)
        {
            Normmutter = 2;

            Lbl_Nenndurchmesser.Visibility = Visibility.Visible;
            Cb_Nenndurchmesser.Visibility = Visibility.Visible;
            Lbl_Material.Visibility = Visibility.Visible;
            Cb_Material.Visibility = Visibility.Visible;
            Lbl_Stückzahl.Visibility = Visibility.Visible;
            Tb_Stückzahl.Visibility = Visibility.Visible;

            Lbl_Gewindeart.Visibility = Visibility.Visible;
            Cb__Gewindeart.Visibility = Visibility.Visible;
            Lbl_Gewindesteigung.Visibility = Visibility.Visible;
            Tb_Gewindesteigung.Visibility = Visibility.Visible;
            Lbl_Gewinderichtung.Visibility = Visibility.Visible;
            Cb_Gewinderichtung.Visibility = Visibility.Visible;

            Lbl_Höhe.Visibility = Visibility.Visible;
            Lbl_Schlüsselweite.Visibility = Visibility.Visible;
            Tb_Schlüsselweite.Visibility = Visibility.Visible;
            Tb_Höhe.Visibility = Visibility.Visible;

        }

        //ENDE NUTZERFÜHRUNG

        


        
        // Ab Hier beginnt die Farbliche Eingabenüberprüfung

        //START FÄRBUNG und Eingabeüberprüfung
        private void Tb_Schlüsselweite_TextChanged(object sender, RoutedEventArgs e) 
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res))
            {
                tb.Background = Brushes.LightGreen;
                tb.BorderBrush = Brushes.Green;
                Schlüsselweite = Convert.ToDouble(tb.Text);
            }
            else if (Normmutter == 0 || Normmutter == 1)
            {
                Fehler = 0;
            }
            else
            {
                tb.Background = Brushes.LightPink;
                tb.BorderBrush = Brushes.Red;
                Fehler = 1;
            }
            if (Schlüsselweite >= 1.2 * Nenndurchmesser)
            {
                Fehler = 1;
            }
        }
        private void Tb_Höhe_TextChanged(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res) && (Normmutter == 1 || Normmutter == 2))
            {
                tb.Background = Brushes.LightGreen;
                tb.BorderBrush = Brushes.Green;
                Höhe = Convert.ToDouble(tb.Text);
            }
            else if (Normmutter == 0)
            {
                Fehler = 0;
            }
            else
            {
                tb.Background = Brushes.LightPink;
                tb.BorderBrush = Brushes.Red;
                Fehler = 1;
            }
        }
        private void Tb_Stückzahl_TextChanged(object sender, RoutedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res))
            {
                Stückzahl = Convert.ToInt32(tb.Text);
                tb.Background = Brushes.LightGreen;
                tb.BorderBrush = Brushes.Green;

            }
            else
            {
                tb.Background = Brushes.LightPink;
                tb.BorderBrush = Brushes.Red;
                Fehler = 1;
            }
        }
        private void Tb_Gewindesteigung_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = (TextBox)sender;
            double res;

            if (Double.TryParse(tb.Text, out res) && (Normmutter == 2))
            {
                tb.Background = Brushes.LightGreen;
                tb.BorderBrush = Brushes.Green;
                Gewindesteigung = Convert.ToDouble(tb.Text);
            }
            else if (Normmutter == 0 || Normmutter == 1)
            {
                Fehler = 0;
            }
            else
            {
                tb.Background = Brushes.LightPink;
                tb.BorderBrush = Brushes.Red;
                Fehler = 1;
            }
        }
        //ENDE Färbung und Eingabeüberprüfung

        //START EINGABE COMBOBOXEN

        //START Nenndurchmessereingabe

        /*//Wenn möglich Prüfen, funktioniert nicht wie es soll
        public void Cbi_Selected(object sender, RoutedEventArgs e)
        {
            Double[] NenndurchmesserArray = new double[18] { 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 27, 30, 33, 36 };

            ComboBoxItem cbi = (ComboBoxItem)sender;
            cbi = Convert.ToInt(Tag);
            //NenndurchmesserInt = Cbi_M2.
        }*/

        private void Cbi_M2_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 0;
            Nenndurchmesser = 2;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M3_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 1;
            Nenndurchmesser = 3;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M4_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 2;
            Nenndurchmesser = 4;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M5_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 3;
            Nenndurchmesser = 5;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M6_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 4;
            Nenndurchmesser = 6;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M8_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 5;
            Nenndurchmesser = 8;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M10_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 6;
            Nenndurchmesser = 10;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M12_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 7;
            Nenndurchmesser = 12;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M14_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 8;
            Nenndurchmesser = 14;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M16_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 9;
            Nenndurchmesser = 16;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M18_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 10;
            Nenndurchmesser = 18;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M20_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 11;
            Nenndurchmesser = 20;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M22_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 12;
            Nenndurchmesser = 22;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M24_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 13;
            Nenndurchmesser = 24;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        private void Cbi_M27_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 14;
            Nenndurchmesser = 27;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M30_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 15;
            Nenndurchmesser = 30;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M33_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 16;
            Nenndurchmesser = 33;
            Cbi__Trapezgewinde.Visibility = Visibility.Collapsed;
        }
        private void Cbi_M36_Selected(object sender, RoutedEventArgs e)
        {
            NenndurchmesserInt = 17;
            Nenndurchmesser = 36;
            Cbi__Trapezgewinde.Visibility = Visibility.Visible;
        }
        //ENDE Nenndurchmessereingabe

        //START Materialeingabe

        /*//WICHTIG ZU PRÜFEN
        public void Cbi_MaterialSelected(object sender, RoutedEventArgs e)
        {
            ComboBoxItem cbi = (ComboBoxItem)sender;
            cbi = Convert.ToInt(Tag);

        }
        public void Cbi_Material_Selected(object sender, RoutedEventArgs e)
        {
            Material = Convert.ToInt32(Tag);
        }*/
        private void Cbi_Stahl_Selected(object sender, RoutedEventArgs e)
        {
            Material = 0;
        }
        private void Cbi_V2A_Selected(object sender, RoutedEventArgs e)
        {
            Material = 1;
        }
        private void Cbi_V4A_Selected(object sender, RoutedEventArgs e)
        {
            Material = 2;
        }
        private void Cbi_Messing_Selected(object sender, RoutedEventArgs e)
        {
            Material = 3;
        }
        private void Cbi_Aluminium_Selected(object sender, RoutedEventArgs e)
        {
            Material = 4;
        }
        private void Cbi_Titan_Selected(object sender, RoutedEventArgs e)
        {
            Material = 5;
        }

        //ENDE Materialeingabe
        //START Gewindearteingabe
        private void Cbi_RegelSelected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 0;
        }
        private void Cbi_FeinSelected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 1;
        }
        private void Cbi_TrapezSelected(object sender, RoutedEventArgs e)
        {
            Gewindeart = 2;
        }
        //ENDE Gewindearteingabe
        //START Gewinderichtung

        private void Cbi_Rechtsdrehend_Selected(object sender, RoutedEventArgs e)
        {
            Gewinderichtung = 0;
        }

        private void Cbi_Linksdrehend_Selected(object sender, RoutedEventArgs e)
        {
            Gewinderichtung = 1;
        }

        //ENDE Gewinderichtungeingabe

        public void calcAll(Mutter mutter)
        {
            try
            {

                Gewicht = mutter.getGewicht();
                Volumen = mutter.getVolumen();
                Gewindesteigung = mutter.getGewindesteigung();
                Schlüsselweite = mutter.getSchlüsselweite();
                Höhe = mutter.getHöhe();
                Flankendurchmesser = FlankendurchmesserArrayRegel[Cb_Nenndurchmesser.SelectedIndex];
                Gewindetiefe = mutter.getGewindetiefe();
                maxDurchmesser = mutter.maxGewindedurchmesser;
                minDurchmesser = mutter.minGewindedurchmesser;
                // Preis = mutter.getPreis() * (int)Lbl_Stückzahl.GetValue(int);
            }
            catch (System.NullReferenceException ex)
            {
                // Diese Fehlermeldung bedeutet, dass es diese Mutter in der Liste nicht gibt
                // Beispielsweise gibt es die M4 als Sechskant und Hut, jedoch nicht als Vierkant oder Flansch
                switch (Mutterntyp)
                {
                    case 0:
                        Typ = "Sechskant";
                        break;
                    case 1:
                        Typ = "Vierkant";
                        break;
                    case 2:
                        Typ = "Flansch";
                        break;
                    case 3:
                        Typ = "Hut";
                        break;
                }
                MessageBox.Show("Die Mutter " + Cb_Nenndurchmesser.Text + " gibt es nicht als " + Typ + "mutter. Bitte andere Mutter auswählen.");
            }

        }

        private void Cb_Nenndurchmesser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
