﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WpfApp1
{
    public class Mutter
    {
        //Eigenschaften der Mutter
        //Eingaben
        //public int Abbruchoption;
        public String Name;
        public int Normmutter;
        public int Mutterntyp;
        public int Gewindeart;
        public int Gewinderichtung;
        public int Material;
        public int NenndurchmesserInt;
        public double Nenndurchmesser;
        public int Stückzahl;


        //Eingabe oder Berechnung
        public double Gewindesteigung;
        public double Schlüsselweite;
        public double Höhe;


        //reine Berechnung

        public double maxDurchmesser;
        public double Volumen;
        public double Gewicht;
        public double Preis;
        public double minGewindedurchmesser;
        public double maxGewindedurchmesser;
        public double Gewindetiefe;
        public double Gewinderundung;
        public double Flankendurchmesser;
        public double PreisProStück;

        //Hilfsvariablen
        public double[] Dichte = new double[7] { 0, 7.85, 7.9, 8.0, 8.73, 2.7, 4.507 };   //Dichte in g/cm^3
        public double[] Materialpreis = new double[7] { 0, 2, 3, 4, 3.5, 4, 10 };          //Euro pro Gramm (Angaben sind ausgedacht)
        public Boolean Fehler;

        public Mutter(double nenndurchmesser, double schlüsselweite, double maxGewindedurchmesser, double höhe, double gewindesteigung, double Flankendurchmesser, double Gewindetiefe) // Konstruktor
        {
            this.Nenndurchmesser = nenndurchmesser;
            this.Schlüsselweite = schlüsselweite;
            this.maxGewindedurchmesser = maxGewindedurchmesser;
            this.Höhe = höhe;
            this.Gewindesteigung = gewindesteigung;
            this.Flankendurchmesser = Flankendurchmesser;
            this.Gewindetiefe = Gewindetiefe;
        }

        // Überladener Konstruktor für Viekantmutter und Flanschmutter
        public Mutter(double nenndurchmesser, double schlüsselweite, double maxGewindedurchmesser, double höhe, double gewindesteigung)
        {
            this.Nenndurchmesser = nenndurchmesser;
            this.Schlüsselweite = schlüsselweite;
            this.maxGewindedurchmesser = maxGewindedurchmesser;
            this.Höhe = höhe;
            this.Gewindesteigung = gewindesteigung;
        }

        // Überladener Konstruktor für Hutmutter
        public Mutter(double nenndurchmesser, double schlüsselweite, double maxGewindedurchmesser, double höhe, double gewindesteigung, double Gewindetiefe)
        {
            this.Nenndurchmesser = nenndurchmesser;
            this.Schlüsselweite = schlüsselweite;
            this.maxGewindedurchmesser = maxGewindedurchmesser;
            this.Höhe = höhe;
            this.Gewindesteigung = gewindesteigung;
            this.Gewindetiefe = Gewindetiefe;
        }

        public Mutter() // Konstruktor
        {

        }

        public double calcHöhe(int typ)
        {
            Höhe = this.Höhe;
            switch (typ)
            {
                case 0:
                    return Höhe = Math.Round((0.7899870974 * this.Nenndurchmesser) + 0.8325114838, 2);
                case 1:
                    return Höhe = Math.Round((0.8197127937 * this.Nenndurchmesser) - 0.0432114883, 2);
                case 2:
                    return Höhe = Math.Round((0.7899870974 * this.Nenndurchmesser) + 0.8325114838, 2);
                case 3:
                    return Höhe += 2;
            }
            return Höhe;
        }

        public double calcSchlüsselweite(int typ)
        {
            this.Schlüsselweite = 0;
            switch (typ)
            {
                case 0:
                    this.Schlüsselweite = Math.Round((1.430519228 * this.Nenndurchmesser) + 1.936218626, 2);
                    break;
                case 1:
                    this.Schlüsselweite = Math.Round((1.462140992 * this.Nenndurchmesser) + 1.25848564, 2);
                    break;
                case 2:
                    this.Schlüsselweite = Math.Round((1.462140992 * this.Nenndurchmesser) + 1.25848564, 2);
                    break;
                case 3:
                    this.Schlüsselweite = Math.Round((1.430519228 * this.Nenndurchmesser) + 1.936218626, 2);
                    break;
            }
            return this.Schlüsselweite;
        }


        public double calcGewindeSteigung()
        {
            Gewindesteigung = 0;
            // Berechnung Spezialanfertigung
            Gewindesteigung = this.Nenndurchmesser * 0.1;
            return Gewindesteigung;
            // Teil für Spezifizierbar fehlt noch
        }


        public double calcmaxDurchmesser(int typ)
        {
            this.maxDurchmesser = 0;
            switch (typ)
            {
                case 0:
                    this.maxDurchmesser = Math.Round(this.Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                    break;
                case 1:
                    this.maxDurchmesser = Math.Sqrt(Math.Pow(this.Schlüsselweite, 2) + Math.Pow(this.Schlüsselweite, 2));
                    break;
                case 2:
                    this.maxDurchmesser = Math.Round(this.Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                    break;
                case 3:
                    this.maxDurchmesser = Math.Round(this.Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                    break;
            }
            return this.maxDurchmesser;

        }

        public double calcVolumen(double maxDurchmesser, double maxGewindedurchmesser, double minGewindedurchmesser, double Schlüsselweite, int typ, int auswahl)
        {
            switch (auswahl)
            {
                case 0:
                    switch (typ)
                    {
                        case 0:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        case 1:
                            Volumen = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);  //Welches Problem gab es damit?
                            break;
                        case 2:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2)));
                            break;
                        case 3:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7);
                            break;
                    }
                    break;
                case 1: // Case 1 = Spezifi 

                    switch (typ)
                    {
                        case 0:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        case 1:
                            Volumen = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);  //Welches Problem gab es damit?
                            break;
                        case 2:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2)));
                            break;
                        case 3:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7);
                            break;
                    }
                    break;

                case 2: // Spezial

                    switch (typ)
                    {
                        case 0:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        case 1:
                            Volumen = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        case 2:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2)));
                            break;
                        case 3:
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7);
                            break;
                    }
                    break;
            }
            return Volumen*-1;
        }

        public double calcGewicht(int typ, int material)
        {

            switch (typ)
            {
                case 0:
                    this.Gewicht = Math.Round(this.Höhe * (((3 * Math.Sqrt(3) * this.maxDurchmesser) / (8)) - (Math.PI * Math.Pow((((this.maxGewindedurchmesser - this.minGewindedurchmesser) / 2) + this.minGewindedurchmesser), 2))), 2) * (Dichte[material + 1] / 1000);
                    break;
                case 1:
                    this.Gewicht = Math.Round(this.Höhe * ((this.Schlüsselweite * this.Schlüsselweite) - (Math.PI * Math.Pow(((this.maxGewindedurchmesser - this.minGewindedurchmesser) / 2) + this.minGewindedurchmesser, 2))), 2) * (Dichte[material + 1] / 1000);
                    break;
                case 2:
                    this.Gewicht = Math.Round(this.Höhe * ((3 * Math.Sqrt(3) * this.maxDurchmesser / 8) - (Math.PI * Math.Pow(((this.maxGewindedurchmesser - this.minGewindedurchmesser) / 2) + this.minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(this.Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((this.Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(this.Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(this.Schlüsselweite / 2, 3) - 2) / 2))) * (Dichte[material + 1] / 1000);
                    break;
                case 3:
                    this.Gewicht = Math.Round(this.Höhe * ((3 * Math.Sqrt(3) * this.maxDurchmesser / 8) - (Math.PI * Math.Pow(((this.maxGewindedurchmesser - this.minGewindedurchmesser) / 2) + this.minGewindedurchmesser, 2))), 2) + (((Math.Pow((this.maxDurchmesser + 5) / 2, 2) * Math.PI) - this.maxGewindedurchmesser) * 1.7) * (Dichte[material + 1] / 1000);
                    break;
            }
            if(this.Gewicht < 0)
            {
                this.Gewicht *= -1;
            }
            return this.Gewicht;
        }


        
        

        
         
                    
                     



        public Boolean überprüfe()
        {
            //prüfen ob Schlüsselweite kleiner als Kernloch ist etc.
            //provisorisch
            Fehler = false;

            return Fehler;
        }
        public void setNormmutter(int x)
        {
            Normmutter = x;

        }

        public void setMutterntyp(int x)
        {
            Mutterntyp = x;

        }

        public void setGewindeart(int x)
        {
            Gewindeart = x;

        }

        public void setGewinderichtung(int x)
        {
            Gewinderichtung = x;

        }

        public void setMaterial(int x)
        {
            Material = x;

        }

        public void setmaxDurchmesser(double x)
        {
            maxDurchmesser = x;

        }


        public void setNenndurchmesser(Double x)
        {
            Nenndurchmesser = x;

        }

        public void setStückzahl(int x)
        {
            Stückzahl = x;

        }

        public void setGewindesteigung(Double x)
        {
            Gewindesteigung = x;

        }

        public void setSchlüsselweite(Double x)
        {
            Schlüsselweite = x;

        }

        public void setHöhe(Double x)
        {
            Höhe = x;

        }



        //get Methoden
        public Double getNormmutter()
        {
            return Normmutter;
        }

        public int getMutterntyp()
        {
            return Mutterntyp;

        }

        public int getGewindeart()
        {
            return Gewindeart;

        }

        public int getGewinderichtung()
        {
            return Gewinderichtung;

        }

        public int getMaterial()
        {
            return Material;

        }

        public Double getNenndurchmesser()
        {
            return this.Nenndurchmesser;

        }

        public int getStückzahl()
        {
            return Stückzahl;

        }

        public Double getGewindesteigung()
        {
            return Gewindesteigung;

        }

        public Double getSchlüsselweite()
        {
            return Schlüsselweite;

        }

        public Double getHöhe()
        {
            return Höhe;

        }

        public Double getmaxDurchmesser()
        {
            return maxDurchmesser;

        }

        public Double getVolumen()
        {
            return Volumen;

        }

        public Double getGewicht()
        {
            return Gewicht;

        }

        public Double getPreis()
        {
            return Preis;

        }

        public Double getminGewindedurchmesser()
        {
            return minGewindedurchmesser;

        }

        public Double getmaxGewindedurchmesser()
        {
            return maxGewindedurchmesser;

        }

        public Double getGewindetiefe()
        {
            return Gewindetiefe;

        }

        public Double getGewinderundung()
        {
            return Gewinderundung;

        }

        public Double getFlankendurchmesser()
        {
            return Flankendurchmesser;

        }



        //Berechnungen

        public void calcSchlüsselweite()
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            
                            break;
                        case 2:             //Spezifische Größe
                            Schlüsselweite = Math.Round((1.430519228 * Nenndurchmesser) + 1.936218626, 2);

                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Schlüsselweite = Math.Round((1.462140992 * Nenndurchmesser) + 1.25848564, 2);

                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Schlüsselweite = Math.Round((1.462140992 * Nenndurchmesser) + 1.25848564, 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:
                            
                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Schlüsselweite = Math.Round((1.430519228 * Nenndurchmesser) + 1.936218626, 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:
                            
                            break;
                    }

                    break;
                default:

                    break;
            }
        }

        public void calcHöhe()
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Höhe = Math.Round((0.7899870974 * Nenndurchmesser) + 0.8325114838, 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Höhe = Math.Round((0.8197127937 * Nenndurchmesser) - 0.0432114883, 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Höhe = Math.Round((0.7899870974 * Nenndurchmesser) + 0.8325114838, 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Höhe += 2;
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }
        /*
        public void calcGewindesteigung() //DIESE METHODE UM EIN DRITTEL KÜRZEN;D GEWINDE SIND GENORMT                      //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Gewindesteigung = Nenndurchmesser * 0.1;
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Gewindesteigung= Nenndurchmesser * 0.1;
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Gewindesteigung= Nenndurchmesser * 0.1;
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                          Gewindesteigung= Nenndurchmesser * 0.1;
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }
        */

        /*
         * 
                                           
         * 
         * 
         * 
         * */

        /*
        public void calcmaxDurchmesser()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxDurchmesser = Math.Round(Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxDurchmesser = Math.Sqrt(Math.Pow(Schlüsselweite, 2) + Math.Pow(Schlüsselweite, 2));
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxDurchmesser = Math.Round(Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxDurchmesser = Math.Round(Schlüsselweite / Math.Sin(60 * Math.PI / 180.0), 2);
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }
        */

        public double calcVolumen()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        case 3:             //Spezialanfertigung
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Volumen = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);  //Welches Problem gab es damit?
                            break;
                        case 3:             //Spezialanfertigung
                            Volumen = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2);
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2)));
                            break;
                        case 3:             //Spezialanfertigung
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2)));
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7);
                            break;
                        case 3:             //Spezialanfertigung
                            Volumen = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7);
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
            if (Volumen < 0)
            {
                Volumen *= -1;
            }
            return Volumen;
        }


        /*
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * */






        public void calcGewicht()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Gewicht = Math.Round(Höhe * (((3 * Math.Sqrt(3) * maxDurchmesser) / (8)) - (Math.PI * Math.Pow((((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser), 2))), 2) * (Dichte[Material + 1] / 1000);
                            break;
                        case 3:             //Spezialanfertigung
                            Gewicht = Math.Round(Höhe * (((3 * Math.Sqrt(3) * maxDurchmesser) / (8)) - (Math.PI * Math.Pow((((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser), 2))), 2) * (Dichte[Material + 1] / 1000);
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Gewicht = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) * (Dichte[Material + 1] / 1000);
                            break;
                        case 3:
                            Gewicht = Math.Round(Höhe * ((Schlüsselweite * Schlüsselweite) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) * (Dichte[Material + 1] / 1000);
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                          Gewicht = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2))) * (Dichte[Material + 1] / 1000);
                            break;
                        case 3:             //Spezialanfertigung
                            Gewicht = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (Math.PI * Math.Pow(Schlüsselweite / 2, 2) * 3) - (Math.PI * Math.Pow((Schlüsselweite / 2) - 2, 2)) + ((4 / 3 * Math.PI * (Math.Pow(Schlüsselweite / 2, 3) / 2)) - (4 / 3 * Math.PI * ((Math.Pow(Schlüsselweite / 2, 3) - 2) / 2))) * (Dichte[Material + 1] / 1000);
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Gewicht = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7) * (Dichte[Material + 1] / 1000);
                            break;
                        case 3:             //Spezialanfertigung
                            Gewicht = Math.Round(Höhe * ((3 * Math.Sqrt(3) * maxDurchmesser / 8) - (Math.PI * Math.Pow(((maxGewindedurchmesser - minGewindedurchmesser) / 2) + minGewindedurchmesser, 2))), 2) + (((Math.Pow((maxDurchmesser + 5) / 2, 2) * Math.PI) - maxGewindedurchmesser) * 1.7) * (Dichte[Material + 1] / 1000);
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }

        //START Berechnung der Gewindeparameter
        public void calcRegelgewinde()
        {
            double[] GewindesteigungArrayRegel = new double[18] {0.40, 0.50, 0.70, 0.80, 1.00, 1.25, 1.50, 1.75, 2.00, 2.00, 2.50, 2.50, 2.50, 3.00, 3.00, 3.50, 3.50, 4.00};
            double[] FlankendurchmesserArrayRegel = new double[18] {1.740, 2.675, 3.545, 4.480, 5.350, 7.188, 9.026, 10.863, 12.701, 14.701, 16.376, 18.376, 20.376, 22.051, 25.051, 27.727, 30.727, 33.402};

            Gewindesteigung = GewindesteigungArrayRegel[NenndurchmesserInt];
            Flankendurchmesser = FlankendurchmesserArrayRegel[NenndurchmesserInt];
            Gewindetiefe = 0.61343 * Gewindesteigung;
        }

        public void calcFeingewinde()
        {
            double[] GewindesteigungArrayFein = new double[18] {0.40, 0.50, 0.70, 0.80, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 2.50, 3.00, 3.00, 3.50, 3.50, 4.00 };

            Gewindesteigung = GewindesteigungArrayFein[NenndurchmesserInt];
            Flankendurchmesser = 0;
            Gewindetiefe = 0.61343 * Gewindesteigung;
        }
        public void calcTrapezgewinde()
        {
            double[] GewindesteigungArrayTrapez = new double[18] { 0, 0, 0, 0, 0, 1.50, 2.00, 3.00, 0, 4.00, 0, 4.00, 0, 5.00, 0, 0, 0, 6.00 };

            Gewindesteigung = GewindesteigungArrayTrapez[NenndurchmesserInt];
            Flankendurchmesser = Nenndurchmesser - (0.5 * Gewindesteigung);
            Gewindetiefe = Gewindesteigung / 1.75;
        }
        //ENDE Berechnung der Gewindeparameter
        public double calcminGewindedurchmesser()      //Unnötig weil eigene Normvorgaben und dann immer gleich, nicht von der Mutter abghängig                 //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        case 3:             //Spezialanfertigung
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        case 3:             //Spezialanfertigung
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        case 3:             //Spezialanfertigung
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        case 3:             //Spezialanfertigung
                            minGewindedurchmesser = Nenndurchmesser;
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
            return minGewindedurchmesser;
        }

        public void calcmaxGewindedurchmesser()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        case 3:             //Spezialanfertigung
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        case 3:             //Spezialanfertigung
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        case 3:             //Spezialanfertigung
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        case 3:             //Spezialanfertigung
                            maxGewindedurchmesser = Nenndurchmesser + 0.51;
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }

        public void calcGewindetiefe()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }

        public void calcGewinderundung()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }

        public void calcFlankendurchmesser()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }

        public void calcPreisProStück()                       //Diese Methode kann für jeden zu berechnenden Wert verwendet werden.
        {
            switch (Mutterntyp)
            {
                case 1:         //Sechskantmutter
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Preis = Gewicht * 2.21;           //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Preis = Gewicht * 2.21;            //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 2:         //Vierkantmutter 
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Preis = Gewicht * 2.21;      //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Preis = Gewicht * 2.21;           //Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 3:         //Mutterntyp 3
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Preis = Gewicht * 2.21;            //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Preis = Gewicht * 2.21;             //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                case 4:         //Mutterntyp 4
                    switch (Normmutter)
                    {
                        case 1:             //Normmutter
                                            //Aus Datenbank auslesen
                            break;
                        case 2:             //Spezifische Größe
                            Preis = Gewicht * 2.21;     //Formel bestimmen
                            break;
                        case 3:             //Spezialanfertigung
                            Preis = Gewicht * 2.21;         //Eventuell Schlüsselweite wird vom Benutzer eingegeben
                            break;
                        default:

                            break;
                    }

                    break;
                default:

                    break;

            }
        }
        
        

    }

    
    

}


